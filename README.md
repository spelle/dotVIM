dotVIM
======

Imported from https://github.com/spelle/dotVIM

I. Clone the repo
-----------------

	git clone git@framagit.org:spelle/dotVIM.git .vim
	cd .vim
	git submodule init
	git submodule update
	cd ..

II . .vimrc
-----------

	ln -s .vim/vimrc .vimrc

III. Install vundle bundles
--------------------------

	vim -c :BundleInstall

YouCompleteMe should fail with message :

	ycm_client_support.[so|pyd|dll] and ycm_core.[so|pyd|dll] not detected; you need to compile YCM before using it. Read the docs!
	Press ENTER or type command to continue

It have to be build. Let's see this later

IV. Powerline for the Airline bundle
------------------------------------

	sudo apt install fonts-powerline

V. YouCompleteMe Bundle compilation
-----------------------------------

	sudo apt install build-essential cmake python-dev

	cd .vim/bundle/YouCompleteMe
	./install.sh --clang-completer

